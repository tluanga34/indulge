var dynamicData = {
	"activities" : [
		{
			"category" : "Adventure",
			"shownItems" : [
				{
					"name" : "DEEP SEA FISHING",
					"imgSrc" : "img/items/adventure/deep_sea_fishing/584x475.jpg",
					"desc" : "Head out on a boat to catch some game fish on a deep sea fishing excursion. Learn to bait, hook and reel!"
				},
				{
					"name" : "Helicopter Joy Rides",
					"imgSrc" : "img/items/adventure/604x230.jpg",
					"desc" : "Hover above the city and appreciate iconic landmarks like never before, through an aerial view. An exhilarating and memorable joy ride!"
				},
				{
					"name" : "Hot Air Ballooning ",
					"imgSrc" : "img/items/adventure/295x230.jpg",
					"desc" : "Get a bird’s view of the city on a hot air balloon ride. The beautiful panoramic views up top offer great photo opportunities!"
				},
				{
					"name" : "River Rafting",
					"imgSrc" : "img/items/adventure/295x230_2.jpg",
					"desc" : "Take on challenging rapids in beautiful landscapes, and let the icy water and fear wash over in this exhilarating water sport!"
				},
			],
			/*"hiddenItems" : [
				{
					"name" : "Skydiving",
					"imgSrc" : "img/items/adventure/skydiving/295x230.jpg",
					"desc" : "We have partnered with the best adventure companies in India to offer adrenaline junkies some unique memorable experiences. From bungee jumping and hot air balloon rides to deep sea fishing and river rafting, we offer it all!"
				},
				{
					"name" : "Skydiving",
					"imgSrc" : "img/items/adventure/skydiving/295x230.jpg",
					"desc" : "We have partnered with the best adventure companies in India to offer adrenaline junkies some unique memorable experiences. From bungee jumping and hot air balloon rides to deep sea fishing and river rafting, we offer it all!"
				},
				{
					"name" : "Skydiving",
					"imgSrc" : "img/items/adventure/skydiving/295x230.jpg",
					"desc" : "We have partnered with the best adventure companies in India to offer adrenaline junkies some unique memorable experiences. From bungee jumping and hot air balloon rides to deep sea fishing and river rafting, we offer it all!"
				},
				{
					"name" : "Skydiving",
					"imgSrc" : "img/items/adventure/skydiving/295x230.jpg",
					"desc" : "We have partnered with the best adventure companies in India to offer adrenaline junkies some unique memorable experiences. From bungee jumping and hot air balloon rides to deep sea fishing and river rafting, we offer it all!"
				},
			]*/
		},
		{
			"category" : "Fine Dining",
			"shownItems" : [
				{
					"name" : "Luxury Dining",
					"imgSrc" : "img/items/finedining/584x475.jpg",
					"desc" : "Tantalize your palette in the lap of luxury. Treat yourself to a gourmet dining experience!"
				},
				{
					"name" : "Food Tour",
					"imgSrc" : "img/items/finedining/604x230.jpg",
					"desc" : "Learn about the local culinary heritage from a guide. Take a stroll around street-food joints and sample the best of local cuisine!"
				},
				{
					"name" : "Luxury Wine Tasting",
					"imgSrc" : "img/items/finedining/295x230.jpg",
					"desc" : "Discover the language of wine connoisseurs as you sample some divine wine. Swirl, sniff, sip and savor!"
				},
				{
					"name" : "Family Dining",
					"imgSrc" : "img/items/finedining/295x230_2.jpg",
					"desc" : "Break bread with your loved ones in some of the best family restaurants in the city. Quality time and food with the family!"
				},
			],
			/*"hiddenItems" : [
				{
					"name" : "Fine Dining",
					"imgSrc" : "img/items/finedining/295x230.jpg",
					"desc" : "Dining experiences at popular restaurants are the perfect way to tantalize the taste buds and make on feel like royalty. Discover the foodie in you with experiences such as fine dining, wine tasting, cooking and much more!"
				},
				{
					"name" : "Fine Dining",
					"imgSrc" : "img/items/finedining/295x230.jpg",
					"desc" : "Dining experiences at popular restaurants are the perfect way to tantalize the taste buds and make on feel like royalty. Discover the foodie in you with experiences such as fine dining, wine tasting, cooking and much more!"
				},
				{
					"name" : "Fine Dining",
					"imgSrc" : "img/items/finedining/295x230.jpg",
					"desc" : "Dining experiences at popular restaurants are the perfect way to tantalize the taste buds and make on feel like royalty. Discover the foodie in you with experiences such as fine dining, wine tasting, cooking and much more!"
				},
				{
					"name" : "Fine Dining",
					"imgSrc" : "img/items/finedining/295x230.jpg",
					"desc" : "Dining experiences at popular restaurants are the perfect way to tantalize the taste buds and make on feel like royalty. Discover the foodie in you with experiences such as fine dining, wine tasting, cooking and much more!"
				},
			]*/
		},
		{
			"category" : "Health & Sports",
			"shownItems" : [
				{
					"name" : "Martial Arts",
					"imgSrc" : "img/items/health&sports/584x475.jpg",
					"desc" : "Get trained in martial arts. Pick up some self-defense moves and boost your fitness!"
				},
				{
					"name" : "Golf",
					"imgSrc" : "img/items/health&sports/604x230.jpg",
					"desc" : "A professional class for golfers of all levels, whether it’s your first stint at golfing or you want to improve your game!"
				},
				{
					"name" : "Group Classes",
					"imgSrc" : "img/items/health&sports/295x230.jpg",
					"desc" : "Participate in fun group classes and meet like-minded people with similar interests as you!"
				},
				{
					"name" : "Personal Trainer",
					"imgSrc" : "img/items/health&sports/295x230_2.jpg",
					"desc" : "Enhance your health, fitness and wellness through a personal training session. Nurture your mind, body and soul."
				},
			],
			/*"hiddenItems" : [
				{
					"name" : "Health & Sports",
					"imgSrc" : "img/items/health&sports/295x230.jpg",
					"desc" : "From golf lessons and kick boxing to yogo and nutrition workshops, Indulge offers you the opportunity to reward your employees, channel partners and customers the gift of health. Incorporate our health & sports experiences in your Wellness platform to create a well-rounded program."
				},
				{
					"name" : "Health & Sports",
					"imgSrc" : "img/items/health&sports/295x230.jpg",
					"desc" : "From golf lessons and kick boxing to yogo and nutrition workshops, Indulge offers you the opportunity to reward your employees, channel partners and customers the gift of health. Incorporate our health & sports experiences in your Wellness platform to create a well-rounded program."
				},
				{
					"name" : "Health & Sports",
					"imgSrc" : "img/items/health&sports/295x230.jpg",
					"desc" : "From golf lessons and kick boxing to yogo and nutrition workshops, Indulge offers you the opportunity to reward your employees, channel partners and customers the gift of health. Incorporate our health & sports experiences in your Wellness platform to create a well-rounded program."
				},
				{
					"name" : "Health & Sports",
					"imgSrc" : "img/items/health&sports/295x230.jpg",
					"desc" : "From golf lessons and kick boxing to yogo and nutrition workshops, Indulge offers you the opportunity to reward your employees, channel partners and customers the gift of health. Incorporate our health & sports experiences in your Wellness platform to create a well-rounded program."
				},
			]*/
		},
		{
			"category" : "Lifestyle",
			"shownItems" : [
				{
					"name" : "Photography Classes",
					"imgSrc" : "img/items/lifestyle/584x475.jpg",
					"desc" : "Learn the art of photography hands-on with lessons from professional shutterbugs. It doesn’t matter if you’re a novice or an expert!"
				},
				{
					"name" : "City Tours",
					"imgSrc" : "img/items/lifestyle/604x230.jpg",
					"desc" : "Take a trip around your city, and discover its heritage and local landmarks. How well do you know your city?"
				},
				{
					"name" : "Music & Dance",
					"imgSrc" : "img/items/lifestyle/295x230.jpg",
					"desc" : "Discover your passion for music and dance through classes. Pick up a new form of dance/music or show off your skills!"
				},
				{
					"name" : "Luxury car hire",
					"imgSrc" : "img/items/lifestyle/295x230_2.jpg",
					"desc" : "Make those hot wheels yours, even if it’s just for a few hours. Feel like royalty as you are taken around in a chauffeur-driven luxury ride around the city!"
				},
			],
			/*"hiddenItems" : [
				{
					"name" : "Lifestyle",
					"imgSrc" : "img/items/lifestyle/295x230.jpg",
					"desc" : "Indulge offers experiences around music, dance, art, craft, photograhpy, city tours, Bollywood tours, luxury car hire and much more! Celebrate your achievements by enjoying a unique experience and create fun memories too."
				},
				{
					"name" : "Lifestyle",
					"imgSrc" : "img/items/lifestyle/295x230.jpg",
					"desc" : "Indulge offers experiences around music, dance, art, craft, photograhpy, city tours, Bollywood tours, luxury car hire and much more! Celebrate your achievements by enjoying a unique experience and create fun memories too."
				},
				{
					"name" : "Lifestyle",
					"imgSrc" : "img/items/lifestyle/295x230.jpg",
					"desc" : "Indulge offers experiences around music, dance, art, craft, photograhpy, city tours, Bollywood tours, luxury car hire and much more! Celebrate your achievements by enjoying a unique experience and create fun memories too."
				},
				{
					"name" : "Lifestyle",
					"imgSrc" : "img/items/lifestyle/295x230.jpg",
					"desc" : "Indulge offers experiences around music, dance, art, craft, photograhpy, city tours, Bollywood tours, luxury car hire and much more! Celebrate your achievements by enjoying a unique experience and create fun memories too."
				},
			]*/
		},
		{
			"category" : "Spa & Salon",
			"shownItems" : [
				{
					"name" : "Body treatments",
					"imgSrc" : "img/items/spa&salon/584x475.jpg",
					"desc" : "Indulge in revitalizing body treatments like body scrubs, body wraps and body polish to cleanse, exfoliate, tone, and nourish your skin."
				},
				{
					"name" : "Massage",
					"imgSrc" : "img/items/spa&salon/604x230.jpg",
					"desc" : "Make your pick from invigorating massage therapies like Ayurvedic and Swedish massages that also promote holistic wellness."
				},
				{
					"name" : "Nails",
					"imgSrc" : "img/items/spa&salon/295x230.jpg",
					"desc" : "From manicures and pedicures to nail enhancements, show your hands and feet some TLC with nail care."
				},
				{
					"name" : "Hair & Beauty",
					"imgSrc" : "img/items/spa&salon/295x230_2.jpg",
					"desc" : "Pamper and preen yourself with a wide range of facial and hair salon  beauty treatments. Look good, feel great!"
				},
			],
			/*"hiddenItems" : [
				{
					"name" : "Spa & Salon",
					"imgSrc" : "img/items/spa&salon/295x230.jpg",
					"desc" : "Relax and unwind as you indulge in luxurious spa & salon treatments at India's best salons. Spa experiences such as Swedish massage and Ayurveda treatments are a great way to recharge one's batteries. One can be groomed to look and feel their best through salon experiences like body wraps and manicures."
				},
				{
					"name" : "Spa & Salon",
					"imgSrc" : "img/items/spa&salon/295x230.jpg",
					"desc" : "Relax and unwind as you indulge in luxurious spa & salon treatments at India's best salons. Spa experiences such as Swedish massage and Ayurveda treatments are a great way to recharge one's batteries. One can be groomed to look and feel their best through salon experiences like body wraps and manicures."
				},
				{
					"name" : "Spa & Salon",
					"imgSrc" : "img/items/spa&salon/295x230.jpg",
					"desc" : "Relax and unwind as you indulge in luxurious spa & salon treatments at India's best salons. Spa experiences such as Swedish massage and Ayurveda treatments are a great way to recharge one's batteries. One can be groomed to look and feel their best through salon experiences like body wraps and manicures."
				},
				{
					"name" : "Spa & Salon",
					"imgSrc" : "img/items/spa&salon/295x230.jpg",
					"desc" : "Relax and unwind as you indulge in luxurious spa & salon treatments at India's best salons. Spa experiences such as Swedish massage and Ayurveda treatments are a great way to recharge one's batteries. One can be groomed to look and feel their best through salon experiences like body wraps and manicures."
				},
			]*/
		},
		{
			"category" : "Weekend Getaway",
			"shownItems" : [
				{
					"name" : "Luxury Hotels",
					"imgSrc" : "img/items/weekendgetaway/584x475.jpg",
					"desc" : "Live like royalty in the finest hotels that are revered for their opulence, gourmet cuisine and unrivalled service!"
				},
				{
					"name" : "Boutique Hotels",
					"imgSrc" : "img/items/weekendgetaway/604x230.jpg",
					"desc" : "Stay in one of our exclusive selection of boutique hotels that offer plush comfort and decadent food, with an attentive staff always at hand!"
				},
				{
					"name" : "Jungle Safaris",
					"imgSrc" : "img/items/weekendgetaway/295x230.jpg",
					"desc" : "Explore the jungle and check out wildlife up-close in their natural habitat through an adventurous safari experience!"
				},
				{
					"name" : "Heritage Forts & Palaces",
					"imgSrc" : "img/items/weekendgetaway/295x230_2.jpg",
					"desc" : "Luxury camps, boutique homestays, plantation bungalows, and private villas by the sea, yoga, wellness and spa retreats. Each hotel is unique and intimate and powered by charismatic hosts and their abiding passions."
				},
			],
			/*"hiddenItems" : [
				{
					"name" : "Weekend getaway",
					"imgSrc" : "img/items/weekendgetaway/295x230.jpg",
					"desc" : "Reward you employees with long service awards at a Taj property and reinforce their loyalty to your organisation. Reward your dealers with enjoyable experiences, breaks at boutique properties, and opportunity that they wouldn't otherwise pursue. What better way to celebrate a milestones and acheivements?"
				},
				{
					"name" : "Weekend getaway",
					"imgSrc" : "img/items/weekendgetaway/295x230.jpg",
					"desc" : "Reward you employees with long service awards at a Taj property and reinforce their loyalty to your organisation. Reward your dealers with enjoyable experiences, breaks at boutique properties, and opportunity that they wouldn't otherwise pursue. What better way to celebrate a milestones and acheivements?"
				},
				{
					"name" : "Weekend getaway",
					"imgSrc" : "img/items/weekendgetaway/295x230.jpg",
					"desc" : "Reward you employees with long service awards at a Taj property and reinforce their loyalty to your organisation. Reward your dealers with enjoyable experiences, breaks at boutique properties, and opportunity that they wouldn't otherwise pursue. What better way to celebrate a milestones and acheivements?"
				},
				{
					"name" : "Weekend getaway",
					"imgSrc" : "img/items/weekendgetaway/295x230.jpg",
					"desc" : "Reward you employees with long service awards at a Taj property and reinforce their loyalty to your organisation. Reward your dealers with enjoyable experiences, breaks at boutique properties, and opportunity that they wouldn't otherwise pursue. What better way to celebrate a milestones and acheivements?"
				},
			]*/
		},
	]	
};