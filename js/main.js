(function(){
	angular.module("indulge",[])
	
	.controller("activities",function($scope){
		
		var activeFrame = 0;
		
		dynamicData.activities.forEach(function(item, index){
			item.uniqueId = index;
			item.expanded = false;
			
			if(index > 0)
				item.shown = false;
			else
				item.shown = true;
		});
		
		$scope.items = dynamicData.activities;
		$scope.interactions = $scope.itemMenu = {};
		
		$scope.itemMenu.getClass = function(e)
		{
			if($scope.items[e].shown == false)
				return "inactive";
			else
				return "active";
		}
		
		$scope.itemMenu.click = function(e)
		{
			$scope.items[activeFrame].shown = false;
			$scope.items[e].shown = true;
			activeFrame = e;
		}
		
		$scope.interactions.showMoreClick = function($event){
			var buttonElem = $event.currentTarget,
				pos = $(buttonElem).attr("data-pos"),
				hiddenContainer = $(".activities-more-showcase"),
				targetHeight = hiddenContainer.eq(pos)[0].scrollHeight,
				speed = 600;
			
			if(hiddenContainer.eq(pos).attr("data-expanded") == "false")
			{
				hiddenContainer.eq(pos).velocity({height : targetHeight + "px"}, speed, function(e){
					$(e).css({"height" : "auto"}).attr("data-expanded","true");
					$(buttonElem).html("Show Less -");
				});
			}
			else
			{
				hiddenContainer.eq(pos).velocity({height : ["0px", targetHeight+"px"]}, speed, function(e){
					$(e).attr("data-expanded","false");
					$(buttonElem).html("Show More +");
				});
			}
			
		};
		
	})
	
	.controller("scroll-navigation",function($scope){
		
		var $navMenuItems = $("#nav-menu-main li"),
			$menuUl = $("#nav-menu-main"),
			$anchorElements = $(".nav-section"),
			$navContainer = $("#top-nav"),
			$headerNav = $("#header-nav"),
			$logoAnchor = $("#logoAnchor"),
			$menuBack = $("#mobileMenubackBtn"),
			bottomPos = $headerNav[0].offsetTop + $headerNav[0].offsetHeight,
			fixed = false,
			speed = 800;
				
		$navMenuItems.each(function(ind, val){
			$(this).attr("data-nav-pos",ind);
		});
		
		$navMenuItems.on("click",function(){
			var pos = $(this).attr("data-nav-pos"),
				scrollPos = $anchorElements.eq(pos).offset().top - $headerNav.height();
			$("html").velocity("scroll", { duration: speed, offset: scrollPos+"px", mobileHA: false });
			
			if(document.documentElement.clientWidth <= 600)
				$scope.mobileMenu.collapse();
			
		});
		
		$logoAnchor.on("click",function(){
			if(window.pageYOffset >= 50)
				$("html").velocity("scroll", { duration: speed, offset: "0px", mobileHA: false });
			else
				console.log("Already on top");
		});
		
		$(window).on("scroll",function(e){
			
			if(window.pageYOffset > bottomPos && !fixed)
			{	
				$headerNav.addClass("fixed").css({"top":"-100%"});
				$headerNav.velocity({top : ["0px", "-"+$headerNav[0].offsetHeight + "px"]}, {duration: 700});
				fixed = true;
			}
			else if(window.pageYOffset <= bottomPos && fixed)
			{
				$headerNav.removeClass("fixed");
				fixed = false;
			}
		});
			
		$(window).on("resize",function(){
			bottomPos = $headerNav[0].offsetTop + $headerNav[0].offsetHeight;
		});
		
		$scope.mobileMenu = {
			"state" : 0,
			"expand" : function(){
				$menuUl.velocity({right: ["0px","-300px"]});
				this.state = 1;
			},
			"collapse" : function(){
				$menuUl.velocity({right: ["-300px","0px"]});
				this.state = 0;
			},
			"addGesture" : function(){
				
				//SWIPE GESTURE TO DISPLAY MENU IN MOBILE DEVICE
				var body = document.body,
					screenX,
					screenY,
					diff,
					diffY,
					timeInterval,
					timer;
				body.addEventListener("touchstart",function(e){
					screenX = e.touches[0].screenX;
					screenY = e.touches[0].screenY;
					timer = diff = diffY = 0;
					timeInterval = setInterval(function(){
						timer++;
					}, 100);
				});
				
				body.addEventListener("touchmove",function(e){
					diff = screenX - e.touches[0].screenX;
					diffY = screenY - e.touches[0].screenY;
				});
				
				body.addEventListener("touchend",function(e){
					clearInterval(timeInterval);
					diffY = Math.abs(diffY);
					
					if(diff > 40 && timer <= 6 && timer >= 1 && diffY < 50 && $scope.mobileMenu.state == 0)
						$scope.mobileMenu.expand();
					else if(diff < -20 && timer <= 6 && timer >= 1 && diffY < 50 && $scope.mobileMenu.state == 1)
						$scope.mobileMenu.collapse();
					// console.log(diffY);
				});
			}
		};
		
		$scope.mobileMenu.addGesture();
	})
	
	.controller("slide-gallery",function($scope){
		
		$scope.banner = {
			pager : true,
			auto : true,
			speed : 2000,
			pause : 6000
		};
		
		$scope.move = function($event){

		}
		
	})
	
	.controller("contact",function($scope,$http){
		
		$scope.showSubmitted = false;
		$scope.showError = false;
		$scope.showLoading = false;
		
		$scope.submitForm = function(e){
			
			$scope.showLoading = true;
		
			var data = {
					"name" : e.currentTarget[0].value,
					"email" : e.currentTarget[1].value,
					"subject" : e.currentTarget[2].value,
					"message" : e.currentTarget[3].value,
					"date" : new Date()
				},
				url = "https://api.mlab.com/api/1/databases/indulge/collections/contactUs?apiKey=RSR_WMJer3hbWwLXk4iBqgYthD4P1rNL";
			
			$http({
				url : url,
				data : data,
				method : "POST",
				headers: {
					'Content-Type': 'application/json',
				}
				
			}).then(
				
				function(sucess){
					
					$scope.showSubmitted = true;
					$scope.showLoading = false;
				},
				
				function(failed){
					
					$scope.showError = true;
					$scope.showLoading = false;
				}
			
			);
			
			
		}
		
	})
	
	.directive("ngTouchmove",function(){
		
		return {
			scope : {
				ngTouchmove : "&"
			},
			restrict : "A",
			link : function(scope,elem,attr){

				elem.on("touchmove",function(e){
					e.stopPropagation();
					e.stopImmediatePropagation();
					scope.ngTouchmove(e);

				})
				
			}
		}
	})
	
	.directive("ngSlider",function(){
		
		return {
			restrict : "A",
			scope : {
				ngSlider : "=",
			},
			link : function(scope, elem, attr){
				
				elem.bxSlider(scope.ngSlider);
			}			
		};
		
	})
	
	.controller("flip-box",function($scope,$http,$timeout,dl){
		
		var activeIndex = null;
		
		$scope.flipBox = {
			
			"status" : flipBoxArray,
			"flipIt" : function(indx){
				
				if(this.status[indx].flipped == false)
					this.status[indx].flipped = true;
				else
					this.status[indx].flipped = false;
				
				if(activeIndex !== null && activeIndex != indx)
					this.status[activeIndex].flipped = false;					
				
				activeIndex = indx;
			}	
		};
		
		$scope.screenHeight = document.documentElement.clientHeight + "px";
		
		var timeout;
		$(window).on("resize",function(){
			clearTimeout(timeout);
			timeout = setTimeout(function(){
				$scope.screenHeight = document.documentElement.clientHeight + "px";
				$scope.$apply();
			},50);
		});
		
		
		$scope.download = '';
		$scope.showLoading = false;
		
		$scope.downloadForm = {
			showError : false,
			url : "",
			"shown" : false,
			"submittedMessage" : false,
			"show" : function($event,dlname){
				this.shown = true;
				//console.log(this.shown);
				$scope.download = dlname;
				$event.stopPropagation();
				dl.url = dl.urlList[dlname];
				$scope.downloadForm.url = dl.url;
				$scope.downloadForm.showError = false;
				$("body").css({"overflow":"hidden"});
			},
			"hide" : function($event){
				this.shown = false;
				$("body").css({"overflow":"auto"});
			},
			"companySize" : {
				"value" : "",
				"class" : 'hide',
				"option" : [
					{
						"value" : "Less than 1,000"
					},
					{
						"value" : "1,000 - 5,000"
					},
					{
						"value" : "5,000 - 10,000"
					},
					{
						"value" : "10,000+"
					},
				]
			},
			"hearingChannel" : {
				"value" : "",
				"class" : 'hide',
				"option" : [
					{
						"value" : "Magazine"
					},
					{
						"value" : "Website"
					},
					{
						"value" : "Advertisements"
					},
					{
						"value" : "Friends"
					},
				]
			}
		};
		
		$scope.stopPropagation = function($event){$event.stopPropagation();};
		
		$scope.submit= function(e){
			var form = e.currentTarget,
				url = "https://api.mlab.com/api/1/databases/indulge/collections/visitorInfo?apiKey=RSR_WMJer3hbWwLXk4iBqgYthD4P1rNL",
				data = {
					"first_name" : form[0].value,
					"last_name" : form[1].value,
					"company" : form[2].value,
					"email" : form[3].value,
					"job" : form[4].value,
					"state" : form[5].value,
					"company_size" : form[6].value,
					"hearing_channel" : form[7].value,
					"resource_download" : $scope.download,
					"date" : new Date()
				};
			
			$scope.showLoading = true;
			
			$http({
				data : data,
				method : "POST",
				url : url,
				headers: {
					'Content-Type': 'application/json',
				}
			}).then(
			
				function(sucess){
					console.log(sucess.data);
					dl.download(dl.url);
					$scope.downloadForm.shown = false;
					$scope.downloadForm.submittedMessage = true;
					$scope.showLoading = false;
				},
				function(failed){
					$scope.downloadForm.showError = true;
					$scope.showLoading = false;
				}
			);
		}
		
	})
	.directive("ngCurrentyear",function(){
		
		return {
			
			restrict : "A",
			link : function(scope, elem, attr){
				elem.html((new Date()).getFullYear());				
			}
			
		}
		
	})
	
	.service("dl",function(){
		
		this.urlList = {
			
			"brochure":"downloads/indulge_final_PDF.pdf",
			"whitepaper" : "downloads/indulge_Whitepappers.zip",
			"presentation" : "downloads/presentation.pdf"
			
		};
		this.url = "";
		
		this.download = function(sUrl){
		
			//Creating new link node.
			var link = document.createElement('a');
			link.href = sUrl;
		 
			if (link.download !== undefined){
				//Set HTML5 download attribute. This will prevent file from opening if supported.
				var fileName = sUrl.substring(sUrl.lastIndexOf('/') + 1, sUrl.length);
				link.download = fileName;
			}
			//Dispatching click event.
			if (document.createEvent) {
				var e = document.createEvent('MouseEvents');
				e.initEvent('click' ,true ,true);
				link.dispatchEvent(e);
				return true;
			}
			// Force file download (whether supported by server).
			var query = '?download';
			window.open(sUrl + query);
		}		
		
	});
	
	$(window).on("load",function(){
		$(".wrapper:eq(0)").css({"opacity":1});
		//$(".loading-anim").remove();
		$(".loading-anim").css({"display":"none"});
	})

})();


 

