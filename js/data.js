var app = angular.module("app",[]);

app.controller("data",function($scope,dataServ){
	
	var scp = $scope;
	
	scp.tab = 1;
	scp.showLoading = true;
	
	scp.contacts = new CONSTRUCT_TABLE("contacts");
	scp.downloads = new CONSTRUCT_TABLE("downloads");
	
	scp.contacts.setUrl("https://api.mlab.com/api/1/databases/indulge/collections/contactUs","?apiKey=RSR_WMJer3hbWwLXk4iBqgYthD4P1rNL");
	scp.contacts.getData();
	
	scp.downloads.setUrl("https://api.mlab.com/api/1/databases/indulge/collections/visitorInfo","?apiKey=RSR_WMJer3hbWwLXk4iBqgYthD4P1rNL");
	
	
	function CONSTRUCT_TABLE(n){
		
		var table_name = "Not Set",
			self = this,
			apiUrl = {
				path : "",
				apiKey : ""
			};
		
		self.data = [];
		self.activeRow = null;
		
		//Constructor that set the name
		if(n)
			table_name = n;
		//Get the name of Table
		self.getName = function(){
			return table_name;
		}
		
		//Set the name of Table
		self.setName = function(name){
			table_name = name;
		}
		
		self.setUrl = function(path, key){
			apiUrl.path = path;
			apiUrl.key = key;
		}
		
		//Get Data from Mongo DB
		self.getData = function(){
			var url = apiUrl.path + apiUrl.key;	
			
			//Send ajax only if data desn't exist yet.
			if(self.data.length <= 0){
				scp.showLoading = true;
				dataServ.getHTTP(url,function(data){
					self.data = data;
					scp.showLoading = false;
				});
			}
		}
		
		self.deleteRow = function(){
			var id = self.data[self.activeRow]._id.$oid
			if(self.activeRow != null){
				var url = apiUrl.path + "/" + id + apiUrl.key;
				//dataServ
				scp.showLoading = true;
				dataServ.deleteHTTP(url,function(e){					
					self.data.splice(self.activeRow,1);		
					scp.showLoading = false;					
				});
			}
		}
	}
});

app.service("dataServ",function($http){
	
	var data = [];
	
	//Get Data from DB
	this.getHTTP = function(url, callbackFunc){
		
		$http({
			url : url,
			method : "GET"
		}).then(
			
			function(sucess){
				data = sucess.data;
				callbackFunc(data);
			}, 
			function(failed){
				console.log("Failed Request Data"+url);
			}
		);
	}
	
	//Delete Row from DB
	this.deleteHTTP  = function(url, callbackFunc){
		
		$http({
			method : "DELETE",
			url : url,
			 headers: {
				"Content-Type": "application/json"
			}
		}).then(
			function(res){
				console.log("Data Deleted");
				callbackFunc(res.data);
			},
			function(res){
				console.log("Error Encountered");
			}
		);
	}
});